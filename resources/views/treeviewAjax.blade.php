<!DOCTYPE html> 

<html> 

<head> 

    <title>Treeview Example</title> 

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" target="_blank" rel="nofollow"  /> 

    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" target="_blank" rel="nofollow"  rel="stylesheet"> 
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css" integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script> 

    <link href="/css/treeview.css" target="_blank" rel="nofollow"  rel="stylesheet"> 

</head> 

<body> 

    <div class="container">      

        <div class="panel panel-primary"> 

            <div class="panel-heading">Manage TreeView</div> 

                <div class="panel-body"> 

                    <div class="row"> 

                        <div class="col-md-6"> 

                            <h3>Tree entry List without ajax </h3> 

                                @include('manageChildAjax',['treeEntries' => $treeEntries])


                        </div> 


                    </div> 

                </div> 

            </div> 

        </div> 

    </div> 

    <script src="/js/treeview.js"></script> 
    <script type="text/javascript">
        function treeViewAjax(id,name){
            $('#post-data'+id).replaceWith('<li id="post-data'+id+'">'+name);            
            console.log(id);
            $.ajax({
                url:"{{'get-tree-ajax'}}",
                type:'get',
                data:{
                    id:id,
                },
                success: function(result)
                {
                    $("#post-data"+id).append(result.html);
                }
            })
        }
    </script>

</body> 

</html> 