<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\TreeEntryM;

class TreeViewC extends Controller
{
    function treeViewWithoutJS(Request $request){

        $treeEntries = TreeEntryM::where('parent_entry_id', '=', 0)->with('entry','parentEntry')->get(); 

        
        return view('treeview',compact('treeEntries')); 

    }

    // trew view with ajax 
    
    function treeViewWitjAJax(Request $request){

        $treeEntries = TreeEntryM::where('parent_entry_id', '=', 0)->with('entry','parentEntry')->get(); 

        return view('treeviewAjax',compact('treeEntries')); 

    }

    // get data with parent_entry_id 

    function getTreeAjax(Request $request){

        $treeEntries = TreeEntryM::where('parent_entry_id', $request->id)->with('entry','parentEntry')->get(); 

        $view = view('manageChildAjax',compact('treeEntries'))->render();
        return response()->json(['html'=>$view]);

        dd($treeEntries);
        return view('treeviewAjax',compact('treeEntries')); 

    }

    
}
